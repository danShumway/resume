require 'asciidoctor';
require 'fileutils';
require 'sass';

#Basic configuration
output = 'public';

#Start by cleaning everything out.
if (Dir.exist?(output))
  FileUtils::remove_dir(output);
end

#Create output directory and copy my main files over
FileUtils::mkdir(output);

def styles (input, output)
  scss = File.read(input);
  engine = Sass::Engine.new(scss, :syntax => :scss);
  css = engine.render();

  File.write(output, css);
end

styles('source/main.scss', output + '/main.css');

#Actual source conversion
Asciidoctor.convert_file(
  'source/main.adoc',
  to_dir: output,
  to_file: 'index.html',
  template_dir: 'compile/templates',
  safe: :unsafe
);

